/* Copyright (C) Sanchit Gulati - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sanchit Gulati <sanchit.gulati@gmail.com>, Feb 2015
 */

#include "FMODAudioEngine.h"

using namespace cocos2d;
void (*Common_Private_Error)(FMOD_RESULT);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #define FILE_PREFIX "file:///android_asset/"
#else
    #define FILE_PREFIX
#endif



#if FMOD_DEBUG
#define FAE_LOG(STRING) cocos2d::log("%s",STRING)
bool ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        if (Common_Private_Error)
        {
            Common_Private_Error(result);
        }
        log("FMOD error %d - %s", result, FMOD_ErrorString(result));
        return false;
    }
    return true;
}
#else
#define FAE_LOG(STRING)
bool ERRCHECK(FMOD_RESULT result){return true;}
#endif


bool FMODAudioEngine::instanceFlag = false;
FMODAudioEngine* FMODAudioEngine::instance = NULL;



FMODAudioEngine* FMODAudioEngine::getInstance()
{
    if(instanceFlag)
    {
        return FMODAudioEngine::instance;
    }
    else
    {
        FMODAudioEngine *pRet = new FMODAudioEngine();
        if (pRet && pRet->init())
        {
            FMODAudioEngine::instance = pRet;
            instanceFlag = true;
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = NULL;
            return NULL;
        }
    }
}

bool FMODAudioEngine::init()
{
    int flag = true;
    system = nullptr;
    ERRCHECK(Studio::System::create(&system)); // Create the Studio System object.

    // Initialize FMOD Studio, which will also initialize FMOD Low Level
    FMOD::System* lowLevelSystem = NULL;
    ERRCHECK(system->initialize(512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0));
    ERRCHECK( lowLevelSystem->setSoftwareFormat(0, FMOD_SPEAKERMODE_DEFAULT, 0) );
    
    scheduleUpdate();
    return flag;
}


bool FMODAudioEngine::loadBank(std::string bankName)
{
#if(CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID)
    auto bankString = FileUtils::getInstance()->fullPathForFilename(bankName.c_str());
#else
    auto bankString = std::String("file:///android_asset/"+bankName);
#endif
    
    FMOD::Studio::Bank* fmodBank = NULL;
    ERRCHECK( system->loadBankFile(bankString.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &fmodBank) );
    banks.push_back(std::make_pair(bankName, fmodBank));
    
    return true;
}

bool FMODAudioEngine::loadEvent(std::string eventName)
{
    FMOD::Studio::EventDescription* playDescription = NULL;
    auto eventString = std::string("event:/"+eventName);
    
    FMOD_GUID id;
    auto flag = system->lookupID(eventString.c_str(), &id);
    
    if(flag != FMOD_OK)
        return false;
    
    ERRCHECK( system->getEvent(eventString.c_str(), &playDescription) );
    ERRCHECK( playDescription->loadSampleData());
    
    FMOD::Studio::EventInstance* eventInstance = NULL;
    ERRCHECK( playDescription->createInstance(&eventInstance) );
    
    events.push_back(std::make_pair(eventName, eventInstance));
    
    return true;
}

void FMODAudioEngine::playEvent(std::string eventName)
{
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            Studio::EventInstance* eventInstance = (*it).second;
            ERRCHECK(eventInstance->start());
        }
    }
}

void FMODAudioEngine::stopEvent(std::string eventName,bool fade)
{
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            Studio::EventInstance* eventInstance = (*it).second;
            if(fade)
                ERRCHECK(eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE));
            else
                ERRCHECK(eventInstance->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT));
        }
    }
}


void FMODAudioEngine::setParam(std::string eventName,std::string paramName,float val)
{
    Studio::EventInstance* eventInstance = nullptr;
    /* Check if event is same from last time */
    /* Gives perfomance boost for repeated variable change */
    if(strcmp(eventName.c_str(),lastEvent.first.c_str()) == 0)
        eventInstance = lastEvent.second;
        
        
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            eventInstance = (*it).second;
            lastEvent = *it;
        }
    }
    
    if(eventInstance != nullptr)
    {
        FMOD::Studio::ParameterInstance* mileParam = NULL;
        ERRCHECK( eventInstance->getParameter(paramName.c_str(), &mileParam) );
        ERRCHECK( mileParam->setValue(val) );
    }
}

float FMODAudioEngine::getParam(std::string eventName, std::string paramName)
{
    float val = 0.0f;
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            Studio::EventInstance* eventInstance = (*it).second;
            FMOD::Studio::ParameterInstance* mileParam = NULL;
            ERRCHECK( eventInstance->getParameter(paramName.c_str(), &mileParam) );
            ERRCHECK( mileParam->getValue(&val));
        }
    }
    return val;
}

void FMODAudioEngine::unloadEvent(std::string eventName)
{
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            Studio::EventInstance* eventInstance = (*it).second;
            ERRCHECK(eventInstance->release());
        }
    }
}

void FMODAudioEngine::unloadAllEvents()
{
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it = events.begin(); it != events.end(); it++)
    {
        Studio::EventInstance* eventInstance = (*it).second;
        ERRCHECK(eventInstance->release());
    }
}

void FMODAudioEngine::uploadBank(std::string bankName)
{
    std::vector<std::pair<std::string,Studio::Bank*>>::const_iterator it;
    for(it = banks.begin(); it != banks.end(); it++)
    {
        if( strcmp((*it).first.c_str(),bankName.c_str()) == 0)
        {
            (*it).second->unload();
        }
    }
}

void FMODAudioEngine::setListenerAttributes(Vec3 forward,Vec3 up)
{
    FMOD_3D_ATTRIBUTES attributes = { { 0 } };
    attributes.forward.x = forward.x;
    attributes.forward.y = forward.y;
    attributes.forward.z = forward.z;
    
    
    attributes.up.x = up.x;
    attributes.up.y = up.y;
    attributes.up.z = up.z;
    
    
    ERRCHECK( system->setListenerAttributes(&attributes) );
}

void FMODAudioEngine::setEventPosition(std::string eventName,Vec3 position)
{
    std::vector<std::pair<std::string,Studio::EventInstance*>>::const_iterator it;
    for(it= events.begin(); it != events.end(); it++)
    {
        if( strcmp((*it).first.c_str(),eventName.c_str()) == 0)
        {
            Studio::EventInstance* eventInstance = (*it).second;
            
            FMOD_3D_ATTRIBUTES attributes = { { 0 } };
            
            attributes.position.x = position.x;
            attributes.position.y = position.y;
            attributes.position.z = position.z;
            
            ERRCHECK( eventInstance->set3DAttributes(&attributes) );
        }
    }
}

void FMODAudioEngine::release()
{
    system->release();
}

void FMODAudioEngine::update(float dt)
{
    system->update();
}

void FMODAudioEngine::mute()
{
    
    FMOD::Studio::Bus *bus;
    system->getBus("bus:/", &bus);
    bus->setMute(true);
    
}

void FMODAudioEngine::unmute()
{
    
    FMOD::Studio::Bus *bus;
    system->getBus("bus:/", &bus);
    bus->setMute(false);
    
}