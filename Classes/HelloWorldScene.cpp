#include "HelloWorldScene.h"

USING_NS_CC;

enum TAG {MENU,MUSIC,MUSICPARAM,AIRCRAFT_EXPLODE,BOSS_SHOOT,MISSILE_HIT,BOSS_BY,BULLET_HIT,SELECT,BACK,MUTE,UNMUTE,UNLOAD_MENU};

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}


HelloWorld::HelloWorld()
{
    
}

HelloWorld::~HelloWorld()
{
    _fmod->uploadBank("Master.bank");
    _fmod->uploadBank("Master.strings.bank");
    log("fmod bank unloaded");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");
    
    //set scale to take whole screen
    sprite->setScaleX(visibleSize.width/sprite->getContentSize().width);
    sprite->setScaleY(visibleSize.height/sprite->getContentSize().height);
    
    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
    
    cocos2d::Vector<MenuItem*> items;
    
    auto lbl0 = Label::createWithTTF("change to menu music", "fonts/Marker Felt.ttf", 21);
    lbl0->setColor(Color3B::BLACK);
    auto item0 = MenuItemLabel::create(lbl0, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item0->setTag(TAG::MENU);
    items.pushBack(item0);
    
    auto lbl1 = Label::createWithTTF("change to game music", "fonts/Marker Felt.ttf", 21);
    lbl1->setColor(Color3B::BLACK);
    auto item1 = MenuItemLabel::create(lbl1, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item1->setTag(TAG::MUSIC);
    items.pushBack(item1);
    
    auto lbl2 = Label::createWithTTF("increase background param", "fonts/Marker Felt.ttf", 21);
    lbl2->setColor(Color3B::BLACK);
    auto item2 = MenuItemLabel::create(lbl2, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item2->setTag(TAG::MUSICPARAM);
    items.pushBack(item2);
    
    auto lbl3 = Label::createWithTTF("play aircraft explode", "fonts/Marker Felt.ttf", 21);
    lbl3->setColor(Color3B::BLACK);
    auto item3 = MenuItemLabel::create(lbl3, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item3->setTag(TAG::AIRCRAFT_EXPLODE);
    items.pushBack(item3);
    
    auto lbl4 = Label::createWithTTF("play boss shoot", "fonts/Marker Felt.ttf", 21);
    lbl4->setColor(Color3B::BLACK);
    auto item4 = MenuItemLabel::create(lbl4, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item4->setTag(TAG::BOSS_SHOOT);
    items.pushBack(item4);
    
    auto lbl5 = Label::createWithTTF("play missile hit", "fonts/Marker Felt.ttf", 21);
    lbl5->setColor(Color3B::BLACK);
    auto item5 = MenuItemLabel::create(lbl5, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item5->setTag(TAG::MISSILE_HIT);
    items.pushBack(item5);
    
    auto lbl6 = Label::createWithTTF("play boss by", "fonts/Marker Felt.ttf", 21);
    lbl6->setColor(Color3B::BLACK);
    auto item6 = MenuItemLabel::create(lbl6, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item6->setTag(TAG::BOSS_BY);
    items.pushBack(item6);
    
    auto lbl7 = Label::createWithTTF("play bullet hit", "fonts/Marker Felt.ttf", 21);
    lbl7->setColor(Color3B::BLACK);
    auto item7 = MenuItemLabel::create(lbl7, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item7->setTag(TAG::BULLET_HIT);
    items.pushBack(item7);
    
    auto lbl8 = Label::createWithTTF("play select", "fonts/Marker Felt.ttf", 21);
    lbl8->setColor(Color3B::BLACK);
    auto item8 = MenuItemLabel::create(lbl8, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item8->setTag(TAG::SELECT);
    items.pushBack(item8);
    
    auto lbl9 = Label::createWithTTF("play back", "fonts/Marker Felt.ttf", 21);
    lbl9->setColor(Color3B::BLACK);
    auto item9 = MenuItemLabel::create(lbl9, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item9->setTag(TAG::BACK);
    items.pushBack(item9);
    
    auto lbl10 = Label::createWithTTF("mute", "fonts/Marker Felt.ttf", 21);
    lbl10->setColor(Color3B::BLACK);
    auto item10 = MenuItemLabel::create(lbl10, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item10->setTag(TAG::MUTE);
    items.pushBack(item10);
    
    auto lbl11 = Label::createWithTTF("unmute", "fonts/Marker Felt.ttf", 21);
    lbl11->setColor(Color3B::BLACK);
    auto item11 = MenuItemLabel::create(lbl11, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item11->setTag(TAG::UNMUTE);
    items.pushBack(item11);
    
    auto lbl12 = Label::createWithTTF("unload menu music", "fonts/Marker Felt.ttf", 21);
    lbl12->setColor(Color3B::BLACK);
    auto item12 = MenuItemLabel::create(lbl12, CC_CALLBACK_1(HelloWorld::menuCallback,this));
    item12->setTag(TAG::UNLOAD_MENU);
    items.pushBack(item12);
    
    
    // create menu, it's an autorelease object
    auto menu = Menu::createWithArray(items);
    menu->setPosition(visibleSize.width*0.50,visibleSize.height*0.50);
    menu->alignItemsVerticallyWithPadding(5);
    this->addChild(menu, 1);
    
    
    
    //FMOD INIT//
    
    _fmod = FMODAudioEngine::getInstance();
    addChild(_fmod);
    
    _fmod->loadBank("Master.bank");
    _fmod->loadBank("Master.strings.bank");
    
    _fmod->loadEvent("Music/menu");
    _fmod->loadEvent("Music/game");
    _fmod->loadEvent("SFX/aircraft_explode");
    _fmod->loadEvent("SFX/boss_shoot");
    _fmod->loadEvent("SFX/missile_hit");
    _fmod->loadEvent("SFX/boss_by");
    _fmod->loadEvent("SFX/bullet_hit");
    _fmod->loadEvent("UI/select");
    _fmod->loadEvent("UI/back");
    
    _fmod->playEvent("Music/menu");
    _fmod->setParam("Music/menu","Progression", 1.0f);
    _fmod->setParam("Music/menu","Pickup", 1.0f);
    
    
    scheduleUpdate();
    
    return true;
}

void HelloWorld::update(float dt)
{
}

void HelloWorld::menuCallback(Ref* pSender)
{
    auto id = dynamic_cast<Node*>(pSender)->getTag();
    switch (id) {
        case TAG::MENU:
        {
            _fmod->stopEvent("Music/game");
            _fmod->playEvent("Music/menu");
            break;
        }
        case TAG::MUSIC:
        {
            _fmod->stopEvent("Music/menu");
            _fmod->playEvent("Music/game");
            break;
        }
        case TAG::MUSICPARAM:
        {
            auto val = _fmod->getParam("Music/game", "level");
            val += 3;
            _fmod->setParam("Music/game", "level", val);
            break;
        }
        case TAG::AIRCRAFT_EXPLODE:
        {
            _fmod->playEvent("SFX/aircraft_explode");
            break;
        }
        case TAG::BOSS_SHOOT:
        {
            _fmod->playEvent("SFX/boss_shoot");
            break;
        }
        case TAG::MISSILE_HIT:
        {
            _fmod->playEvent("SFX/missile_hit");
            break;
        }
        case TAG::BOSS_BY:
        {
            _fmod->playEvent("SFX/boss_by");
            break;
        }
        case TAG::BULLET_HIT:
        {
            _fmod->playEvent("SFX/bullet_hit");
            break;
        }
        case TAG::SELECT:
        {
            _fmod->playEvent("UI/select");
            break;
        }
        case TAG::BACK:
        {
            _fmod->playEvent("UI/back");
            break;
        }
        case TAG::MUTE:
        {
            _fmod->mute();
            break;
        }
        case TAG::UNMUTE:
        {
            _fmod->unmute();
            break;
        }
        case TAG::UNLOAD_MENU:
        {
            _fmod->unloadEvent("Music/menu");
            break;
        }
        default:
        {
            Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            exit(0);
#endif
            break;
        }
    }

}
