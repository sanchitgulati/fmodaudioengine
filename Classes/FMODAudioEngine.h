/* Copyright (C) Sanchit Gulati - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sanchit Gulati <sanchit.gulati@gmail.com>, Feb 2015
 */

#ifndef __FMOD_AUDIO_ENGINE__
#define __FMOD_AUDIO_ENGINE__

#include <stdio.h>
#include "cocos2d.h"
#include "fmod/fmod.hpp"
#include "fmod/fmod_studio.hpp"
#include "fmod/fmod_errors.h"

#define FMOD_DEBUG 0



using namespace FMOD;

class FMODAudioEngine : public cocos2d::Node
{
private:
    Studio::System* system;
    
    std::vector<std::pair<std::string,Studio::Bank*>> banks;
    std::vector<std::pair<std::string,Studio::EventInstance*>> events;
    std::pair<std::string,Studio::EventInstance*> lastEvent; /* Optimization, for repeat variable change */
public:
    static FMODAudioEngine* instance;
    static FMODAudioEngine* getInstance();
    static bool instanceFlag;
    bool init();
    
    
    bool loadBank(std::string bankName);
    bool loadEvent(std::string eventName);
    
    void unloadEvent(std::string eventName);
    void unloadAllEvents();
    void uploadBank(std::string bankName);
    
    void playEvent(std::string eventName);
    void stopEvent(std::string eventName,bool fade = false);
    
    void setParam(std::string eventName,std::string paramName,float val = 1.0);
    float getParam(std::string eventName,std::string paramName);
    
    void setListenerAttributes(cocos2d::Vec3 forward = cocos2d::Vec3::ZERO,cocos2d::Vec3 up = cocos2d::Vec3::ZERO);
    void setEventPosition(std::string eventName,cocos2d::Vec3 position = cocos2d::Vec3::ZERO);
    
    void update(float dt);
    
    void mute();
    void unmute();
    
    void release();
    
};

#endif /* defined(__FMOD_AUDIO_ENGINE__) */
